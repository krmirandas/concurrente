package unam.ciencias.computoconcurrente;
import java.lang.Math;
import java.lang.Iterable;

public class PrimeNumberCalculator implements Runnable{

    private int threads;
    private static int numPrimo;
    private static int start;
    public static boolean result;
    public static int longitudSubInter; //Dividimos el intervalo [2,N-1] en this.threads cantidad de sub intervalos, uno por cada hilo


    public PrimeNumberCalculator() {
        this.threads = 1;
    }

    public PrimeNumberCalculator(int threads) {
        this.threads = threads > 1 ? threads : 1;
    }


    public boolean isPrime(int n) throws InterruptedException {
      numPrimo = n; //Define el numero primo que se usara por instancia
      result = true; //Sera true hasta que se diga lo contrario
      longitudSubInter = n / this.threads; // Dividimos el intervalo entre el numero de hilos que se usaran.

      if (n < 2 && n > 0) { //si pasa por aqui, definitivamente sabemos que no es primo
        return false;
      }

      Thread[] myThreads = new Thread[threads]; //Hacemos un arreglo para meter los hilos que piden.

      int start = 2; //Empezamos en 2

      for (int i = 0; i < threads; i++) { //Avanzamos hasta el total de hilos.
        PrimeNumberCalculator number = new PrimeNumberCalculator();  //Creamos la instancia
        number.start = start; 

        Thread t = new Thread(number); //PrimeNumberCalculator implementa la interfaz Runnable, basicamente le estamos diciendo
        //que correr a través del parametro 
        myThreads[i] = t; // agregamos el hilo al arreglo
        t.start(); //se ejecuta el hilo, lo que esta dentro del run()
        start += longitudSubInter; //Le sumamos la siguiente seccion por procesar
      }

      for (int i = 0; i < myThreads.length; i++) {  
        myThreads[i].join(); //Esperamos que cada proceso acabe
      }

      return result;

    }


    @Override
    public void run(){
      if (result) {
        //Empezamos a contar a partir de 2
        for (int i=2; i <= start + longitudSubInter; i++) {
          if (numPrimo % i == 0 && numPrimo != i) { //Verificamos si aqui, resulta no ser primo
            result = false;
          }
        }
      }
    }


}
